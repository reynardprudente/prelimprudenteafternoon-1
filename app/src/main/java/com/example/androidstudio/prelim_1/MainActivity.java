package com.example.androidstudio.prelim_1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;
import android.widget.TextView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button btn1, btn2;
    EditText n;
    TextView t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn1 = (Button) findViewById(R.id.button);
        btn2 = (Button) findViewById(R.id.button2);
        n = (EditText) findViewById(R.id.editText);
        t = (TextView) findViewById(R.id.textView);
        Random rand = new Random();
        final int N = rand.nextInt(20);





        btn1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String n1 = n.getText().toString();
                final int n2 = Integer.parseInt(n1);


                if (n2 == N) {
                    Toast.makeText(MainActivity.this, "You guessed correctly!", Toast.LENGTH_LONG).show();

                } else if (n2 > N) {
                    Toast.makeText(MainActivity.this, "high!",
                            Toast.LENGTH_LONG).show();

                } else if (n2 < N) {
                    Toast.makeText(MainActivity.this, "low!", Toast.LENGTH_LONG).show();

                }
            }
        });


        btn2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String str = String.valueOf(N);
                t.setText(str);
            }
        });
    }
}



